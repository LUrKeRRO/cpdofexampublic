package com.agiletestingalliance;

public class MinMax {

    public int findMax(int firstItem, int secondItem) {
        if (secondItem > firstItem) {
            return secondItem;
	}
        else {
            return firstItem;
	}
    }

}
