package com.agiletestingalliance;

import static org.junit.Assert.*;
import java.io.*;
import java.lang.String;
import org.junit.Test;

public class CPDOFTest {


    @Test
    public void testDesc() throws Exception {

        String test = new AboutCPDOF().desc();
        assertTrue(test.contains("CP-DOF certification program"));
        
    }

    @Test 
	 public void testDur() throws Exception {

        String test = new Duration().dur();
        assertTrue(test.contains("CP-DOF is designed specifically for corporates"));


    }


	 @Test
    public void testUsefullnessDesc() throws Exception {
	 String test = new Usefulness().desc();
	 assertTrue(test.contains("DevOps is about transformation"));
    }

	@Test
    public void testfindMax1() throws Exception {

	int var = new MinMax().findMax(3,4);
	assertEquals("first",var,4);

	}
	
	 @Test
    public void testfindMax2() throws Exception {

        int var = new MinMax().findMax(5,4);
        assertEquals("first",var,5);

        }


}
